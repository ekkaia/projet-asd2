#ifndef STORAGE_H
#define STORAGE_H

#include "Ticket.h"
#include <unordered_map>
#include <queue>
#include <vector>

typedef std::string luggage;

/**
 * @brief Classe consigne
 * 
 */
class Storage {
	public:
		Storage(int storageSizeMax);
		~Storage();
		bool isStorageFull() const;
		Ticket dropOffLuggage(luggage luggage);
		luggage retrieveLuggage(Ticket ticket);
		int getStorageSize() const;
		void displayQueue() const;
		void displayStorage() const;
	private:
		int storageSizeMax;
		std::unordered_map<std::string, int> ticketForLocker;
		std::vector<luggage> lockersList;
		std::queue<int> lockersQueue;
};

#endif