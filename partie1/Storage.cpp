#include "Storage.h"
#include <iostream>

using namespace std;

/**
 * @brief Construct a new Storage:: Storage object
 * 
 * @param storageSizeMax taille de la consigne
 */
Storage::Storage(int storageSizeMax) : storageSizeMax(storageSizeMax), lockersList(storageSizeMax) {
	for (int i = 0 ; i < Storage::storageSizeMax ; i++) {
		Storage::lockersQueue.push(i);
	}
}

/**
 * @brief Destroy the Storage:: Storage object
 * 
 */
Storage::~Storage() {}

/**
 * @brief taille de la consigne
 * 
 * @return int retourne la taille de la consigne
 */
int Storage::getStorageSize() const {
	return Storage::lockersList.size();
}

/**
 * @brief est ce que la consigne est pleine
 * 
 * @return true la consigne est pleine
 * @return false la consigne n'est pas pleine
 */
bool Storage::isStorageFull() const {
	return Storage::lockersQueue.empty();
}

/**
 * @brief déposer un bagage
 * 
 * @param luggage bagage à déposer
 * @return Ticket ticket correspondant au bagage déposé
 */
Ticket Storage::dropOffLuggage(luggage luggage) {
	Ticket ticket;

	if (!Storage::isStorageFull()) {
		Storage::lockersList[Storage::lockersQueue.front()] = luggage;
		Storage::ticketForLocker.insert({ticket.getKey(), Storage::lockersQueue.front()});
		Storage::lockersQueue.pop();
	} else {
		cout << "Consigne pleine !" << endl;
	}

	return ticket;
}

/**
 * @brief Récupérer un bagage
 * 
 * @param ticket ticket correpondant au bagage déposé
 * @return luggage bagage à récupérer
 */
luggage Storage::retrieveLuggage(Ticket ticket) {
	if (Storage::ticketForLocker.find(ticket.getKey()) != Storage::ticketForLocker.end()) {
		int lockerNumber = Storage::ticketForLocker[ticket.getKey()];
		Storage::ticketForLocker.erase(ticket.getKey());

		luggage luggage = Storage::lockersList[lockerNumber];
		Storage::lockersList[lockerNumber].erase();

		Storage::lockersQueue.push(lockerNumber);

		return luggage;
	} else {
		cout << "Bagage introuvable !" << endl;
		return "";
	}
}

/**
 * @brief affiche la consigne
 * 
 */
void Storage::displayStorage() const {
	cout << "STORAGE:" << endl;
	for (int i = 0 ; i < Storage::lockersList.size() ; i++) {
		cout << Storage::lockersList[i] << endl;
	}
}

/**
 * @brief affiche les casiers libres
 * 
 */
void Storage::displayQueue() const {
	cout << "QUEUE :" << endl;
	cout << lockersQueue.front() << endl;
}