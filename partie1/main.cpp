#include <iostream>
#include <string.h>
#include "Ticket.cpp"
#include "Storage.cpp"

using namespace std;

int main() {
	// Consigne de 5 casiers
	Storage storage(5);

	// bagage valise
	string valise = "valise";

	// Déposer des bagages
	Ticket ticket = storage.dropOffLuggage(valise);
	Ticket ticket2 = storage.dropOffLuggage("malette");
	Ticket ticket3 = storage.dropOffLuggage("patate");

	// Déposer d'autres bagages
	storage.dropOffLuggage("chaussure");
	storage.dropOffLuggage("chapeau");
	storage.dropOffLuggage("bouteille d'eau");

	// Afficher la consigne
	storage.displayStorage();

	// Afficher les clés
	cout << ticket.getKey() << endl;
	cout << ticket2.getKey() << endl;

	// Récupérer un bagage
	storage.retrieveLuggage(ticket);

	// Afficher la consigne
	storage.displayStorage();

	// Récupérer un  bagage
	storage.retrieveLuggage(ticket2);

	// Déposer un bagage
	storage.dropOffLuggage("extincteur");

	// Affiche la consigne
	storage.displayStorage();

	// Déposer un bagage
	storage.dropOffLuggage("pistolet");

	// Affiche la consigne
	storage.displayStorage();

	return 0;
}