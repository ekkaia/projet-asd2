#ifndef TICKET_H
#define TICKET_H

#include <string>

/**
 * @brief Classe ticket
 */
class Ticket {
	public:
		Ticket();
		~Ticket();
		bool isEqual(Ticket const& ticket) const;
		bool isDifferent(Ticket const& ticket) const;
		std::string randomString();
		std::string getKey();
	private:
		std::string key;
};

/**
 * @brief Rédéfinition de ticket pour le rendre compatible avec unordered_map
 */
namespace std {
	template<> struct hash<Ticket> {
		size_t operator()(Ticket k) const {
    		using std::size_t;
   			using std::hash;
   			using std::string;
   			return ((hash<string>()(k.getKey())));
   		}
	};
}

#endif