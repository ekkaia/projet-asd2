#include "Ticket.h"

#include <iostream>
#include <string>
#include <random>

using namespace std;

/**
 * @brief Construct a new Ticket:: Ticket object
 */
Ticket::Ticket() {
	Ticket::key = randomString();
}

/**
 * @brief Destroy the Ticket:: Ticket object
 */
Ticket::~Ticket() {}

/**
 * @brief Clé du ticket
 * 
 * @return string renvoi la clé du ticket
 */
string Ticket::getKey() {
	return Ticket::key;
}

/**
 * @brief Est ce que deux tickets sont égaux
 * 
 * @param ticket ticket à comparer
 * @return true les tickets sont égaux
 * @return false les tickets ne sont pas égaux
 */
bool Ticket::isEqual(Ticket const& ticket) const {
	return key == ticket.key;
}

/**
 * @brief Est ce que deux tickets sont différents
 * 
 * @param ticket ticket à comparer
 * @return true les tickets sont différents
 * @return false les tickets sont égaux
 */
bool Ticket::isDifferent(Ticket const& ticket) const {
	return key != ticket.key;
}

bool operator!=(Ticket const& a, Ticket const& b) {
	return a.isDifferent(b);
}

bool operator==(Ticket const& a, Ticket const& b) {
	return a.isEqual(b);
}

/**
 * @brief Génère une chaine de caractère aléatoire alphanumérique de 12 caractères
 * 
 * @return string Renvoi la chaîne générée
 */
string Ticket::randomString() {
	string str = "";
	int ticketSize = 12;
	static const char alphabet[] =
    	"0123456789"
    	"ABCDEFGHIJKLMNOPQRSTUVWXYZ"
    	"abcdefghijklmnopqrstuvwxyz";

	for (int i = 0 ; i < ticketSize ; i++) {
		str += alphabet[rand() % (sizeof(alphabet) - 1)];
	}

	return str;
}