#ifndef LUGGAGECUBE_H
#define LUGGAGECUBE_H

#include <string>
#include "Luggage.h"

/**
 * @brief Classe de bagage cubique hérité de la classe abstraite bagage
 */
class LuggageCube : public virtual Luggage {
    public:
        LuggageCube(std::string name, int cuboid);
        ~LuggageCube();
        int volume();
};

#endif