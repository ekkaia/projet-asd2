#ifndef VSTORAGE_H
#define VSTORAGE_H

#include "Ticket.h"
#include "Luggage.h"
#include <unordered_map>
#include <unordered_set>
#include <queue>
#include <vector>

/**
 * @brief Structure casier
 */
struct locker {
	int volume;
	Luggage *luggage;
};

/**
 * @brief Structure casiers de même volume
 */
struct lockersOfSameVolume {
	std::queue<locker> lockersQueue;
};

/**
 * @brief Classe vconsigne
 */
class vStorage {
	public:
		vStorage(int storageSizeMax, std::vector<int> volumeRange);
		~vStorage();
		bool isStorageFull() const;
		Ticket dropOffLuggage(Luggage *luggage);
		Luggage *retrieveLuggage(Ticket ticket);
		int findTheRightLocker(Luggage *luggage);
		int getStorageSize() const;
		void displayQueue() const;
		void displayStorage() const;
		void displayVolumeRange() const;
	private:
		int storageSizeMax;
		std::unordered_multiset<int> freeLockersVolume;
		// Hash map ?
		std::unordered_map<int, lockersOfSameVolume> lockersList;
		std::unordered_map<std::string, locker> ticketForLocker;
};

#endif