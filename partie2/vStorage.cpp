#include "vStorage.h"
#include <iostream>
#include <random>
#include <array>

using namespace std;

/**
 * @brief Construct a new v Storage::v Storage object
 * 
 * @param storageSizeMax taille de la consigne
 * @param volumeRange plage de volume des casiers
 */
vStorage::vStorage(int storageSizeMax, vector<int> volumeRange) : storageSizeMax(storageSizeMax) {
	// Crée des casiers
	for (int i = 0 ; i < vStorage::storageSizeMax ; i++) {
		locker locker;
		locker.volume = volumeRange[rand() % volumeRange.size()];
		locker.luggage = nullptr;
		vStorage::freeLockersVolume.insert(locker.volume);

		// Si le volume n'existe pas dans la map
		if (lockersList.find(locker.volume) == lockersList.end()) {
			lockersOfSameVolume lockersOfSameVolume;
			vStorage::lockersList.insert({locker.volume, lockersOfSameVolume});
		}

		// On ajoute le casier à la plage de volume correspondante
		lockersList[locker.volume].lockersQueue.push(locker);
	}
}

/**
 * @brief Destroy the v Storage::v Storage object
 */
vStorage::~vStorage() {}

/**
 * @brief La consigne est elle pleine ?
 * 
 * @return true : consigne pleine
 * @return false : consigne non pleine
 */
bool vStorage::isStorageFull() const {
	return vStorage::freeLockersVolume.empty();
}

/**
 * @brief Trouve s'il existe un casier du bon volume de libre
 * 
 * @param luggage bagage à déposer dans la consigne
 * @return int volume du casier libre optimal ou 0 si aucun casier n'est libre
 */
int vStorage::findTheRightLocker(Luggage *luggage) {
	int volume = luggage->volume();
	int rightVolume = 0;

	for (auto const& itr : vStorage::freeLockersVolume) {
		if (itr > volume) {
			rightVolume = itr;

			if (rightVolume - volume > itr - volume) {
				rightVolume = itr;
			}
		}
	}
	
	if (rightVolume != 0) {
		const auto itr = vStorage::freeLockersVolume.find(rightVolume);
		vStorage::freeLockersVolume.erase(itr);
	}

	return rightVolume;
}	

/**
 * @brief Déposer un bagage dans la consigne
 * 
 * @param luggage bagage à déposer
 * @return Ticket renvoi un ticket correspondant au bagage
 */
Ticket vStorage::dropOffLuggage(Luggage *luggage) {
	int rightVolume = vStorage::findTheRightLocker(luggage);
	Ticket ticket;

	if (!vStorage::isStorageFull() && rightVolume != 0) {
		vStorage::lockersList[rightVolume].lockersQueue.front().luggage = luggage;
		vStorage::ticketForLocker.insert({ticket.getKey(), vStorage::lockersList[rightVolume].lockersQueue.front()});
		vStorage::lockersList[rightVolume].lockersQueue.pop();
	} else {
		cout << "Consigne pleine ou aucun casier de libre pour le volume demandé !" << endl;
	}

	return ticket;
}

/**
 * @brief Récupérer son bagage
 * 
 * @param ticket ticket correspondant au bagage dans la hash map
 * @return Luggage* renvoi un pointeur vers le bagage
 */
Luggage* vStorage::retrieveLuggage(Ticket ticket) {
	if (vStorage::ticketForLocker.find(ticket.getKey()) != vStorage::ticketForLocker.end()) {
		locker locker = vStorage::ticketForLocker[ticket.getKey()];
		vStorage::ticketForLocker.erase(ticket.getKey());

		Luggage* luggage = locker.luggage;
		locker.luggage = nullptr;

		vStorage::freeLockersVolume.insert(locker.volume);
		vStorage::lockersList[locker.volume].lockersQueue.push(locker);

		return luggage;
	} else {
		cout << "Bagage introuvable !" << endl;
		return NULL;
	}
}

/**
 * @brief taille de la consigne
 * 
 * @return int renvoi un entier correspondant à la taille de la consigne
 */
int vStorage::getStorageSize() const {
	return vStorage::storageSizeMax;
}

/**
 * @brief affiche le nombre de casiers libres par volume
 * 
 */
void vStorage::displayQueue() const {
	cout << "QUEUE :" << endl;
	for (auto const& itr : vStorage::lockersList) {
		cout << "Casiers vides du volume " << itr.first << endl;
		cout << "- Taille : " << itr.second.lockersQueue.size() << endl;
	}
}

/**
 * @brief affiche les casiers occupés et leur contenu
 * 
 */
void vStorage::displayStorage() const {
	cout << "OCCUPIED LOCKERS :" << endl;
	for (auto const& itr  : vStorage::ticketForLocker) {
    	cout << "- Ticket : " << itr.first << endl; 
		cout << "- Bagage : " << itr.second.luggage->getName() << endl;
		cout << "~~" << endl;
	}
}

/**
 * @brief affiche les plages de volumes supportés par les casiers de la consigne
 * 
 */
void vStorage::displayVolumeRange() const {
	cout << "VOLUME RANGE :" << endl;
	for (auto const& itr : vStorage::lockersList) {
		cout << itr.first << endl;
	}
}