#include "LuggageCube.h"
#include <iostream>
#include <string>

using namespace std;

/**
 * @brief Construct a new Luggage Cube:: Luggage Cube object
 * 
 * @param name nom du bagage
 * @param cuboid dimensions du bagage
 */
LuggageCube::LuggageCube(string name, int cuboid) : Luggage(name, cuboid, cuboid, cuboid)
{}

/**
 * @brief Destroy the Luggage Cube:: Luggage Cube object
 */
LuggageCube::~LuggageCube() {}

/**
 * @brief Calcule le volume du bagage
 * 
 * @return int Renvoi le volume du bagage
 */
int LuggageCube::volume() {
    return LuggageCube::getWidth() * LuggageCube::getHeight() * LuggageCube::getLength();
}