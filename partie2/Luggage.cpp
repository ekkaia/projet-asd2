#include "Luggage.h"
#include <iostream>
#include <string>

using namespace std;

/**
 * @brief Construct a new Luggage:: Luggage object
 * 
 * @param name nom du bagage
 * @param width longueur du bagage
 * @param length largeur du bagage
 * @param height hauteur du bagage
 */
Luggage::Luggage(string name, int width, int length, int height) : name(name), width(width), height(height), length(length) {}

/**
 * @brief Destroy the Luggage:: Luggage object
 */
Luggage::~Luggage() {}

int Luggage::getWidth() {
    return Luggage::width;
}

int Luggage::getHeight() {
    return Luggage::height;
}

int Luggage::getLength() {
    return Luggage::length;
}

string Luggage::getName() {
	return Luggage::name;
}