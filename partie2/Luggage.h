#ifndef LUGGAGE_H
#define LUGGAGE_H

#include <string>

/**
 * @brief Classe bagage abstraite
 */
class Luggage {
    public:
        Luggage(std::string name, int width, int length, int height);
        virtual ~Luggage();
        virtual int volume() = 0;
        int getWidth();
        int getLength();
        int getHeight();
		std::string getName();
    private:
		std::string name;
        int width, length, height;
};

#endif