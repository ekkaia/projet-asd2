#include <iostream>
#include <string.h>
#include <vector>
#include "Ticket.cpp"
#include "vStorage.cpp"
#include "Luggage.cpp"
#include "LuggageCube.cpp"

using namespace std;

int main() {
	// Volume de casiers
	vector<int> volumeRange = {10, 20, 30};
	// Crée une nouvelle consigne
	vStorage storage(8, volumeRange);

	// Bagages
	Luggage *valise = new LuggageCube("valise", 3);
	Luggage *sac = new LuggageCube("sac", 2);
	Luggage *guitar = new LuggageCube("guitar", 3);
	Luggage *lunettes = new LuggageCube("lunettes", 2);
	Luggage *bagage = new LuggageCube("bagage", 3);
	Luggage *pistolet = new LuggageCube("pistolet", 3);

	// Déposer des bagages
	Ticket ticket = storage.dropOffLuggage(valise);
	Ticket ticket2 = storage.dropOffLuggage(lunettes);
	Ticket ticket3 = storage.dropOffLuggage(bagage);
	Ticket ticket4 = storage.dropOffLuggage(sac);
	Ticket ticket5 = storage.dropOffLuggage(guitar);

	// Trace d'exécution
	storage.displayStorage();
	storage.displayVolumeRange();
	storage.displayQueue();

	// Déposer un bagage
	Ticket ticket6 = storage.dropOffLuggage(pistolet);

	// Trace d'exécution
	storage.displayStorage();
	storage.displayVolumeRange();
	storage.displayQueue();

	// Récupérer un bagage
	storage.retrieveLuggage(ticket);

	// Trace d'exécution
	storage.displayQueue();

	return 0;
}